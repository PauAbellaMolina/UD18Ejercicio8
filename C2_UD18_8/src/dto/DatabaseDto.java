package dto;

public class DatabaseDto {

	static String nombre = "";
	
	public DatabaseDto(String nombre) {
		
		this.nombre = nombre;
		
	}
	
	public String getNombre() {
		
		return nombre;
		
	}
	
	public void setNombre(String nombre) {
		
		this.nombre = nombre;
		
	}
	
}
