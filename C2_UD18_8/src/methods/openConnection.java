package methods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class openConnection {
	
	public static Connection conexion;

	public static void openConnection() {
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.226:3306?useTimezone=true&serverTimezone=UTC", "remote", "remoteMysql-217");
			System.out.println("Server Connected");
			
		} catch (SQLException | ClassNotFoundException ex) {
			
			System.out.println("No se ha podido conectar con mi base de datos");
			System.out.println(ex.getMessage());
						
		} 
		
	}
	
}
