import java.sql.Connection;
import methods.*;
import dto.*;

public class GrandesAlmacenesApp {
	
	public static Connection conexion;

	public static void main(String[] args) {
		
		openConnection.openConnection();
		
		DatabaseDto newDatabase = new DatabaseDto("Grandes_Almacenes");
		createDB.createDB(newDatabase);
		
		TableDto newTable1 = new TableDto(newDatabase, "Cajeros");
		String newValue1 = "CREATE TABLE `Cajeros` (`Codigo` int NOT NULL AUTO_INCREMENT, `NomApels` varchar(255) DEFAULT NULL, PRIMARY KEY (`Codigo`)) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
		createTable.createTable(newDatabase, newTable1, newValue1);
		insertData.insertData(newDatabase, "Cajeros", "(Codigo, NomApels) VALUE(1, 'Jaeden');");
		insertData.insertData(newDatabase, "Cajeros", "(Codigo, NomApels) VALUE(2, 'Kerri');");
		insertData.insertData(newDatabase, "Cajeros", "(Codigo, NomApels) VALUE(3, 'Theodora');");
		insertData.insertData(newDatabase, "Cajeros", "(Codigo, NomApels) VALUE(4, 'Dania');");
		insertData.insertData(newDatabase, "Cajeros", "(Codigo, NomApels) VALUE(5, 'Naseem');");
		
		TableDto newTable2 = new TableDto(newDatabase, "Productos");
		String newValue2 = "CREATE TABLE `Productos` (`Codigo` int NOT NULL AUTO_INCREMENT, `Nombre` varchar(100) DEFAULT NULL, `Precio` int DEFAULT NULL, PRIMARY KEY (`Codigo`)) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
		createTable.createTable(newDatabase, newTable2, newValue2);
		insertData.insertData(newDatabase, "Productos", "(Codigo, Nombre, Precio) VALUE(1, 'Escritorio', 99);");
		insertData.insertData(newDatabase, "Productos", "(Codigo, Nombre, Precio) VALUE(2, 'Tostadora', 43);");
		insertData.insertData(newDatabase, "Productos", "(Codigo, Nombre, Precio) VALUE(3, 'Sof�', 83);");
		insertData.insertData(newDatabase, "Productos", "(Codigo, Nombre, Precio) VALUE(4, 'Microondas', 78);");
		insertData.insertData(newDatabase, "Productos", "(Codigo, Nombre, Precio) VALUE(5, 'Jarr�n', 87);");
		
		TableDto newTable3 = new TableDto(newDatabase, "Maquinas_Registradoras");
		String newValue3 = "CREATE TABLE `Maquinas_Registradoras` (`Codigo` int NOT NULL AUTO_INCREMENT, `Piso` int DEFAULT NULL, PRIMARY KEY (`Codigo`)) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
		createTable.createTable(newDatabase, newTable3, newValue3);
		insertData.insertData(newDatabase, "Maquinas_Registradoras", "(Codigo, Piso) VALUE(1, 1);");
		insertData.insertData(newDatabase, "Maquinas_Registradoras", "(Codigo, Piso) VALUE(2, 1);");
		insertData.insertData(newDatabase, "Maquinas_Registradoras", "(Codigo, Piso) VALUE(3, 1);");
		insertData.insertData(newDatabase, "Maquinas_Registradoras", "(Codigo, Piso) VALUE(4, 1);");
		insertData.insertData(newDatabase, "Maquinas_Registradoras", "(Codigo, Piso) VALUE(5, 2);");
		
		TableDto newTable4 = new TableDto(newDatabase, "Venta");
		String newValue4 = "CREATE TABLE `Venta` (`Cajero` int NOT NULL, `Maquina` int NOT NULL, `Producto` int NOT NULL, PRIMARY KEY (`Cajero`,`Maquina`,`Producto`), KEY `Maquina_idx` (`Maquina`), KEY `Producto_idx` (`Producto`), CONSTRAINT `Cajero` FOREIGN KEY (`Cajero`) REFERENCES `Cajeros` (`Codigo`), CONSTRAINT `Maquina` FOREIGN KEY (`Maquina`) REFERENCES `Maquinas_Registradoras` (`Codigo`), CONSTRAINT `Producto` FOREIGN KEY (`Producto`) REFERENCES `Productos` (`Codigo`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
		createTable.createTable(newDatabase, newTable4, newValue4);
		insertData.insertData(newDatabase, "Venta", "(Cajero, Maquina, Producto) VALUE(1, 1, 1);");
		insertData.insertData(newDatabase, "Venta", "(Cajero, Maquina, Producto) VALUE(2, 2, 2);");
		insertData.insertData(newDatabase, "Venta", "(Cajero, Maquina, Producto) VALUE(3, 3, 3);");
		insertData.insertData(newDatabase, "Venta", "(Cajero, Maquina, Producto) VALUE(4, 4, 4);");
		insertData.insertData(newDatabase, "Venta", "(Cajero, Maquina, Producto) VALUE(5, 5, 5);");
		
		closeConnection.closeConnection();

	}

}
